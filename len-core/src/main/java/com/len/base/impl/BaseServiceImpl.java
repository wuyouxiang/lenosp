package com.len.base.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zhuxiaomeng
 * @date 2017/12/13.
 * @email lenospmiller@gmail.com
 * update by 2019/11/12 tkmapper替换成mybatisplus
 */
@Slf4j
public class BaseServiceImpl<E extends BaseMapper, T> extends AbstractServiceImpl<E, T> {

}
